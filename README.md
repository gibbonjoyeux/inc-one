# inc-one

**inc-one** build a single output file from your sources.

## install

```
npm install inc-one -g
```

## Usage

```
> inc-one [FLAGS] [DIRECTORY | CONFIG_FILE]
> inc-one COMMAND
```

Calling **inc-one** without any parameter will make the program search for a
`one.json` config file into your directory. If the config file is not found, it
will search on parent directory and so on.

You can specify a `directory` in which **inc-one** will search for the
`one.json` config file or a path to the `config file` itself (which can then be
named as you want as long as it is a `.json`).

### flags

- +minify	disable output minification
- +mangle	disable output name mangling
- +ascii	disable output unicode characters escaping

### commands

- clean		remove output file
- help		print usage

## How it works

The main aim of **inc-one** is to allow user to **include** files content into
anothers. It simplies development when you want a single file output (like a
library) made of multiple ones.
It works for both `javascript` and `typescript` files.
Here's an example.

### file_1.js
```
function	my_function() {
	console.log("my function !");
}
```

### main.js
```
// include file_1.js
{{ "file_1.js" }}

my_function();
```

Running **inc-one** will replace the `{{ "file_1.js" }}` by it's content into
`main.js`.

### output.js
```
// include file_1.js
function	my_function() {
	console.log("my function !");
}

my_function();
```

Of course, included file can also contain other files.

## one.json

To tell **inc-one** how to handle your project, you need to provide a simple
`one.json` file.

### one.json example
```
{
	"main": "src/main.js",
	"output": "output.js",
	"minify": true,
	"mangle": true
}
```

`main` defines which file is your main file. This file will include other files
(which can then include other ones etc.). The output will be based on this file.

`output` defines the output file path.

### Options

Other values can be added to the `inc-one.json` file.

- minify: activate or not minification (default to `true`)
- mangle: activate or not minification names "mangling" (default to `true`)
- ascii:  unicode characters are escaped (default to `true`)

### Typescript

If you are working in `typescript`, you must specify your output as a `.ts`
file. **inc-one** will then build the typescript file, call `tsc` to compile it
into the corresponding `.js` file and minify it (if enabled).
