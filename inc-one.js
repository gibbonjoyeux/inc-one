#!/usr/bin/env node
/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

"use strict";

////////////////////////////////////////////////////////////////////////////////
/// MODULES
////////////////////////////////////////////////////////////////////////////////

const	fs			= require("fs");
const	path		= require("path");
const	uglify		= require("uglify-js");
const	{ exec }	= require("child_process");

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

const	DEFAULT_CONFIG_FILE	= "one.json";
const	REG_INCLUDE			= /\{\{\s*"(.+?)"\s*}}/g;

let		main;
let		config_file;
let		config;
let		output;
let		ext;

let		minify;
let		minify_options;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

function	error(message, print_usage = false) {
	console.log(`inc-one: ${message}`);
	if (print_usage == true) {
		usage();
	}
	process.exit(1);
}

function	usage() {
	console.log("Usage: inc-one [COMMAND] [FLAGS] [DIRECTORY | CONFIG_FILE]");
	console.log("  commands:");
	console.log("    clean    clean output");
	console.log("    help     print inc-one usage");
	console.log("  flags:");
	console.log("    +minify  do not minify output");
	console.log("    +mangle  do not mangle output's names");
	console.log("    +ascii   do not escape unicode characters");
}

//////////////////////////////////////////////////
/// CONFIG + ARGUMENTS
//////////////////////////////////////////////////

function	handle_arguments() {
	let		argv;
	let		arg;
	let		file;
	let		config_file;

	argv = process.argv.slice(2);
	for (arg of argv) {
		/// COMMANDS
		if (arg == "clean") {
			if (fs.existsSync(config.output) == true) {
				fs.unlinkSync(config.output);
			}
			if (path.extname(config.output) == ".ts") {
				config.output = config.output.slice(0, -3) + ".js";
				if (fs.existsSync(config.output) == true) {
					fs.unlinkSync(config.output);
				}
			}
			process.exit();
		} else if (arg == "help") {
			usage();
			process.exit();
		/// FLAGS
		} else if (arg == "+minify") {
		} else if (arg == "+mangle") {
		} else if (arg == "-ascii") {
		/// PATH
		} else if (fs.existsSync(arg) == true) {
			file = fs.lstatSync(arg);
			/// CONFIG FILE
			if (file.isFile() == true) {
				if (path.extname(arg) == ".json") {
					config_file = path.basename(arg);
					process.chdir(path.dirname(arg));
				} else {
					error(`"${arg}", config file must be .json`, true)
				}
			/// DIRECTORY
			} else if (file.isDirectory()) {
				process.chdir(arg);
				if (config_file == null) {
					config_file = DEFAULT_CONFIG_FILE;
				}
			}
		/// ERROR
		} else {
			error(`"${arg}" is not a directory, a config file, a flag or a command`, true);
		}
	}
	/// NO SPECIFIED CONFIG FILE
	if (config_file == null) {
		config_file = DEFAULT_CONFIG_FILE;
		find_directory();
	}
	return (config_file);
}

function	find_directory() {
	let		prev_dir;

	prev_dir = null;
	while (fs.existsSync(DEFAULT_CONFIG_FILE) == false) {
		process.chdir("../");
		if (prev_dir == process.cwd()) {
			error("Cannot find one.json");
		}
		prev_dir = process.cwd();
	}
}

function	load_config(config_file) {
	config = JSON.parse(fs.readFileSync(config_file));
	minify = true;
	minify_options = {};
	if (config.ascii == true) {
		minify_options.output = {"ascii_only": true};
	}
	if (config.minify == false) {
		minify = false;
	}
	if (config.mangle == false) {
		minify.options.mangle = false;
	}
	ext = config.output.match(/.+\.(.+)$/)[1];
}

function	handle_typescript(output) {
	/// CREATE TS FILE
	fs.writeFileSync(config.output, output);
	/// CREATE JS FILE (WITH TSC)
	exec(`tsc ${config.output}`, function (err, stdout, stderr) {
		if (err) { error(err); }
		if (stderr.length > 0) { error(stderr); }
		config.output = config.output.slice(0, -3) + ".js";
		output = fs.readFileSync(config.output).toString("utf-8");
		compute_output(output);
	});
}

//////////////////////////////////////////////////
/// INCLUDE
//////////////////////////////////////////////////

function	compute_output(output) {
	/// MINIFY
	if (minify == true) {
		output = uglify.minify(output, minify_options);
		/// ERROR
		if (output.error) {
			error(output.error);
		}
		output = output.code;
	}
	/// WRITE FILE
	fs.writeFileSync(config.output, output);
}
function	compute_includes(file_name) {
	let		content;
	let		includes, include;
	let		include_file;
	let		include_content;
	let		file_path;

	file_path = path.dirname(file_name) + "/";
	content = fs.readFileSync(file_name).toString("utf-8");
	includes = content.matchAll(REG_INCLUDE);
	for (include of includes) {
		include_file = file_path + include[1];
		include_content = compute_includes(include_file);
		content = content.replace(include[0], include_content);
	}
	return (content);
}

////////////////////////////////////////////////////////////////////////////////
/// MAIN
////////////////////////////////////////////////////////////////////////////////

/// HANDLE ARGUMENTS
config_file = handle_arguments();
/// LOAD CONFIG
load_config(config_file);
/// COMPUTE INCLUDES
output = compute_includes(config.main);
/// OUTPUT
if (path.extname(config.output) == ".ts") {
	handle_typescript(output);
} else {
	compute_output(output);
}

